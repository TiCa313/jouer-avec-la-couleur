//background avec laurent en cach
$(function () {
    $('input').on("input", function () {
        var r1 = $('#r1').val();
        var g1 = $('#g1').val();
        var b1 = $('#b1').val();
        var r2 = $('#r2').val();
        var g2 = $('#g2').val();
        var b2 = $('#b2').val();
        $('body').css("background", "linear-gradient(50deg, rgb(" + r1 + "," + g1 + "," + b1 + ")60%, rgb(" + r2 + "," + g2 + "," + b2 + "))");
    })
})

//exemple tuto js
// exécute la fonction une fois chargé
const ready = () => {
  let red = 130
  let green = 130
  let blue = 130
  const input = document.querySelectorAll("input");
  const display = document.querySelector(".display");
  
  //boucle dans le input  
  for (var i = 0; i < input.length; i++) {
    input[i].addEventListener("input", function () {
      
      //attribuer une valuer
      red = document.getElementById("red").value
      green = document.getElementById("green").value
      blue = document.getElementById("blue").value;
      
      // attribuer un style pour la couleur
      display.style.background = "rgb(" + red + ", " + green + ", " + blue + ")";
      
      // attribuer une valeur pour la couleur
      outputValue(Number(red), Number(green), Number(blue))

    });
  }
  
    //valeur de sortie lorsque l' imput est sélectionnée 
  const outputValue = (r, g, b) => {
    red = r
    green = g
    blue = b
    const hex = document.querySelector('.hex')
    const rgb = document.querySelector('.rgb')
    hex.textContent = convertToHex(r, g, b)
    rgb.textContent = `(${r},${g},${b})`
    return hex
  }
  
  //convertir rgb en hex 
  const convertToHex = (r, g, b) => {
    let result = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    return result
  }
  
  //ajouter des palettes de couleurs
  const addPalette = () => {
    const btn = document.querySelector('.save')
    const palette = document.querySelector('.palettes')
    btn.addEventListener('click', ()=>{
      const newEl = document.createElement('div')
      newEl.classList.add('placer')
      newEl.innerHTML = `
        <div class="pick">
          <div class='close'>x</div>
          <div class="col" style='background:${convertToHex(red, green, blue)}'></div>
          <div class="colstamp">
            <p>Hex: ${convertToHex(red, green, blue)}</p>
            <p>RGB: (${red},${green},${blue})</p>
          </div>
        </div>`
      
      //newEl.classList.add('fadeIn')
      palette.appendChild(newEl)
      setTimeout(()=>{
        newEl.classList.add('fadeIn')
      },300)
      
      const collections = document.querySelectorAll('.placer')

      //passer la souris et déplacer le bouton de fermeture
      collections.forEach(col =>{
        col.addEventListener('mouseover', ()=>{
          col.firstElementChild.firstElementChild.style.display = 'block'
        })
        col.addEventListener('mouseout', () => {
          col.firstElementChild.firstElementChild.style.display = 'none'
        })
      })
      removePalettes(collections)
    })
  }
  
  //supprimer les palettes
  const removePalettes = (cols) => {
    cols.forEach((col, i )=> {
      col.addEventListener('click', (e)=> {
        col.classList.add('fadeOut')
        remove(col)
      })
    })  
    //retirer l'élément après 300milisec
    function remove(item){
      setTimeout(() => {
        item.parentNode.removeChild(item)
      }, 300)
    }
  }
  addPalette()
}
  
ready()


